<?php

namespace App\Http\Controllers;

use App\Model\PointModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Helper\Dijkstra;

class PointController extends Controller
{
    public $pointModel;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->pointModel = new PointModel();
    }

    public function list()
    {
        $points = $this->pointModel::pointList();
        return response()->json([
            'status' => 200,
            'data' => $points
        ]);
    }

    public function show($id)
    {
        $point = $this->pointModel::pointShow($id);
        return response()->json([
            'status' => 200,
            'data' => $point
        ]);
    }

    public function store(Request $request) {
        $fieldsAllowed = ['name'];

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
            ]);
        }
        
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:points'],
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }

        if($this->pointModel::pointStore($request->all())) {
            return response()->json([
                'status' => 200,
                'message' => 'Data successfully saved'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'message' => 'Data not save'
            ]);
        }
    }

    public function update(Request $request, $id) {
        $fieldsAllowed = ['name'];
      
        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
            ]);
        }
        
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:points'],
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }

        $response = $this->pointModel::pointUpdate($request->all(), $id);
       
        if($response['success'] == true) {
            return response()->json([
                'status' => 200,
                'message' => 'Data successfully updated'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'message' => $response['message']
            ]);
        }
    }

    public function destroy($id) {
        $response = $this->pointModel::pointDestroy($id);
       
        if($response['success'] == true) {
            return response()->json([
                'status' => 200,
                'message' => 'Data successfully deleted'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'message' => $response['message']
            ]);
        }
    }
}
