<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fieldsAllowed = ['first_name', 'last_name', 'email', 'rule', 'username', 'password', 'password_confirmation'];

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
            ]);
        }

        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' =>  ['required', 'string', 'max:255'],
            'email' =>  ['required', 'string', 'email', 'max:255', 'unique:users'],
            'rule' =>  ['required', 'string', 'max:255'],
            'username' =>  ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
        
        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }

        if(strToLower($request->input('rule')) != 'user' && strToLower($request->input('rule')) != 'admin') {
            return response()->json([
                'status' => 406,
                'message' => 'Validation failed',
                'error' => ['Rule failed value must be admin or user']
            ]);
        }
       
        $data = $request->all();
        $data['active'] =  1;
        $data['rule'] =  strToLower($data['rule']);
        $data['password'] =  Hash::make($request->input('password'));

        $user = User::create($data);
        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json([
            'status' => 200,
            'message' => 'Data successfully saved',
            'accessToken' => $accessToken
        ]);
        
    }

    public function login(Request $request)
    {
        $fieldsAllowed = ['username', 'password'];

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
                'fields' => $fieldsAllowed
            ]);
        }

        $validator = Validator::make($request->all(), [
            'username' =>  ['required'],
            'password' => ['required']
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }
       
        if(!auth()->attempt($request->all())) {
            return response()->json([
                'status' => 500,
                'message' => 'Invalid credentials'
            ]);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response()->json([
            'status' => 200,
            'message' => 'Login successfully',
            'access_token' => $accessToken
        ]);
   }

}
