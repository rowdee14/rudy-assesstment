<?php

namespace App\Http\Controllers;

use App\Model\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function profileShow() {
        return response()->json([
            'status' => 200,
            'data' => auth()->user()
        ]);
    }

    public function profileUpdate(Request $request) {
        $fieldsAllowed = ['first_name', 'last_name', 'email', 'username', 'password'];
        $validatorList = [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' =>  ['required', 'string', 'max:255'],
            'email' =>  ['required', 'string', 'email', 'max:255', 'unique:users'],
            'rule' =>  ['required', 'string', 'max:255'],
            'username' =>  ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8']
        ];
        $validatorData = [];
        $data = $request->all();

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
            ]);
        }
        
    
        foreach($fieldsAllowed as $field) {
            if (isset($data[$field])) {
                $validatorData[$field] =  $validatorList[$field];
            }
        }

        $validator = Validator::make($request->all(), $validatorData);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }
       
        if (isset($data[$field])) {
            $data['password'] =  Hash::make($request->input('password'));
        }
        
        User::where('id', auth()->user()->id)->update(array_filter($data));
        $user = User::find(auth()->user()->id);

        return response()->json([
            'status' => 200,
            'message' => 'Data is successfully updated',
            'data' => $user
        ]);        

    }

    public function list() {
        if(strtolower(auth()->user()->rule) != 'admin') {
            return response()->json([
                'status' => 400,
                'message' => 'Admin only can access this route'
            ]);
        }
        return response()->json([
            'status' => 200,
            'data' => User::all()
        ]);
    }

    public function show($id) {
        if(strtolower(auth()->user()->rule) != 'admin') {
            return response()->json([
                'status' => 400,
                'message' => 'Admin only can access this route'
            ]);
        }
        return response()->json([
            'status' => 200,
            'data' => User::find($id)
        ]);
    }

    public function update(Request $request, $id) {
        if(strtolower(auth()->user()->rule) != 'admin') {
            return response()->json([
                'status' => 400,
                'message' => 'Admin only can access this route'
            ]);
        }
      
        $fieldsAllowed = ['first_name', 'last_name', 'email', 'rule', 'username', 'password'];
        $validatorList = [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' =>  ['required', 'string', 'max:255'],
            'email' =>  ['required', 'string', 'email', 'max:255', 'unique:users'],
            'rule' =>  ['required', 'string', 'max:255'],
            'username' =>  ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8']
        ];
        $validatorData = [];
        $data = $request->all();
        $data = array_filter($data);

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 400,
                'message' => 'Not acceptable fields',
            ]);
        }
    
        foreach($fieldsAllowed as $field) {
            if (isset($data[$field])) {
                $validatorData[$field] =  $validatorList[$field];
            }
        }

        $validator = Validator::make($request->all(), $validatorData);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }

        if (isset($data['rule'])) {
            if(strToLower($request->input('rule')) != 'user' && strToLower($request->input('rule')) != 'admin') {
                return response()->json([
                    'status' => 406,
                    'message' => 'Validation failed',
                    'error' => ['Rule failed value must be admin or user']
                ]);
            }
        }

        if (isset($data['password'])) {
            $data['password'] =  Hash::make($request->input('password'));
        }
        
        User::where('id', auth()->user()->id)->update($data);
        $user = User::find(auth()->user()->id);

        return response()->json([
            'status' => 200,
            'message' => 'Data is successfully updated',
            'data' => $user
        ]);        

    }



   public function destroy($id) {
        if(strtolower(auth()->user()->rule) != 'admin') {
            return response()->json([
                'status' => 400,
                'message' => 'Admin only can access this route'
            ]);
        }

        $user = User::find($id);
        if($user != null) {
            $user->delete();
            return response()->json([
                'status' => 200,
                'message' => 'Data is successfully deleted'
            ]);
        }
        else {
            return response()->json([
                'status' => 200,
                'message' => 'No data to be deleted'
            ]);
        }
    }
}