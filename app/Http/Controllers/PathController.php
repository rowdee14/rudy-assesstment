<?php

namespace App\Http\Controllers;

use App\Model\RouteModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PathController extends Controller
{
    
    
    public $routeModel;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->routeModel = new RouteModel();
    }

    public function list()
    {
        $routes = $this->routeModel::routeList();
        return response()->json($routes);
    }

    public function show($id) {
        $route = $this->routeModel::routeShow($id);
        if($route != null) {
            return response()->json([
                'status' => 200,
                'data' => $route
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'data' => $route
            ]);
        }
    }

    public function checker(Request $request) {
      
        $fieldsAllowed = ['origin', 'destination'];

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
            ]);
        }

        $validator = Validator::make($request->all(), [
            'origin' => ['required'],
            'destination' => ['required']
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }

        $data = $this->routeModel::routeChecker($request->all());
        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function store(Request $request) {
        $fieldsAllowed = ['from', 'to', 'time', 'cost'];

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
            ]);
        }
        
        $validator = Validator::make($request->all(), [
            'from' => ['required'],
            'to' => ['required'],
            'time' => ['required'],
            'cost' => ['required'],
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }
       
        $response = $this->routeModel::routeStore($request->all());
        if($response['success'] == true) {
            return response()->json([
                'status' => 200,
                'message' => 'Data successfully saved'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'message' => $response['message']
            ]);
        }
    }

    public function update(Request $request, $id) {
        $fieldsAllowed = ['from', 'to', 'time', 'cost'];

        if ($request->except($fieldsAllowed)) {
            return response()->json([
                'status' => 406,
                'message' => 'Not acceptable fields',
            ]);
        }
        
        $validator = Validator::make($request->all(), [
            'from' => ['required'],
            'to' => ['required'],
            'time' => ['required'],
            'cost' => ['required'],
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Validation failed',
                'error' => $validator->errors()
            ]);
        }
        

        if($request->input('from') == $request->input('to')) {
            return response()->json([
                'status' => 400,
                'message' => 'FROM id must equal TO id',
            ]);
        }
       
        $response = $this->routeModel::routeUpdate($request->all(), $id);
        if($response['success'] == true) {
            return response()->json([
                'status' => 200,
                'message' => $response['message']
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'message' => $response['message']
            ]);
        }
    }

    public function destroy($id) {
        $response = $this->routeModel::routeDestroy($id);
       
        if($response['success'] == true) {
            return response()->json([
                'status' => 200,
                'message' => 'Data successfully deleted'
            ]);
        }
        else {
            return response()->json([
                'status' => 500,
                'message' => $response['message']
            ]);
        }
    }
}
