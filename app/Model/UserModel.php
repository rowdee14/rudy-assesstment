<?php

namespace App\Model;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserModel extends Model
{
    protected $table = 'users';
    public static $tableName = 'users';

    public static function list() {
        $displayFields = ['first_name', 'last_name', 'gender', 'email', 'department', 'type', 'active'];
        return DB::table(self::$tableName)->select($displayFields)->paginate(10);
    }

    public static function show($id) {
        $displayFields = ['first_name', 'last_name', 'gender', 'email', 'department', 'type', 'active'];
        return DB::table(self::$tableName)->select($displayFields)->find($id);
    }

    public static function store($request) {
        $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'gender' => $request->input('gender'),
            'contact_no' => $request->input('contact_no'),
            'email' => $request->input('email'),
            'department' => $request->input('department'),
            'type' => 'User',
            'active' => 1,
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            
        ];
        if(DB::table(self::$tableName)->insert($data)) {
            return true;
        }
        return false;
    }

    // public static function update($request) {
    //     $data = [
    //         'first_name' => $request->input('first_name'),
    //         'last_name' => $request->input('last_name'),
    //         'gender' => $request->input('gender'),
    //         'contact_no' => $request->input('contact_no'),
    //         'email' => $request->input('email'),
    //         'department' => $request->input('department'),
    //         'type' => 'User',
    //         'active' => 1,
    //         'username' => $request->input('username'),
    //         'password' => Hash::make($request->input('password')),
            
    //     ];
    //     // if(DB::table(self::$tableName)->where('id', $request->input('user_id'))->update($data)) {
    //     //     return true;
    //     // }
    //     // return false;

    //     return $request->all();
    // }
}
