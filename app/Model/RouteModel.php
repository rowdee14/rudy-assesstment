<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Fisharebest\Algorithm\Dijkstra;

class RouteModel extends Model
{
    protected $table = 'routes';
    public static $tablePoints= 'points';
    public static $tableRoutes= 'routes';

    public static function routeList() {
        $routes = [];
        $points = DB::table(self::$tablePoints)->get();

        foreach($points as $key => $row) {
            $routesPoint = DB::table(self::$tableRoutes)
            ->join('points', 'routes.to', '=', 'points.id')
            ->select('points.name', 'routes.time', 'routes.cost')
            ->where('routes.from', $row->id)
            ->get();

            $routesPointList = array();

            foreach($routesPoint as $rowKey => $rowValue) {
                array_push($routesPointList, array(
                    'name' => $rowValue->name,
                    'time' => $rowValue->time,
                    'cost' => $rowValue->cost
                ));
            }

            $routes[$row->name] = $routesPointList;
        }

        return $routes;
    }

    public static function routeShow($id) {
        $points = DB::table(self::$tablePoints)->find($id);
        
        if($points !== null) {
            $routesPoint = DB::table(self::$tableRoutes)
            ->join('points', 'routes.to', '=', 'points.id')
            ->select('points.name', 'routes.time', 'routes.cost')
            ->where('routes.from', $points->id)
            ->get();

            $routesPointList = array();

            foreach($routesPoint as $rowKey => $rowValue) {
                array_push($routesPointList, array(
                    'name' => $rowValue->name,
                    'time' => $rowValue->time,
                    'cost' => $rowValue->cost
                ));
            }

            return $routesPointList;
        }
        return null;
    }

    public static function routeStore($data) {
        $data['created_at'] = Carbon::now();
        $row =  DB::table(self::$tableRoutes)
        ->where([
            [self::$tableRoutes.'.from', '=', $data['from']],
            [self::$tableRoutes.'.to', '=', $data['to']]
        ])->count();

        if($row > 0) {
            return ['success' => false, 'message' => 'Route is already exist'];
        }

        if(DB::table(self::$tableRoutes)->insert($data)) {
            return ['success' => true, 'message' => 'Route successfully saved'];
        }
        return ['success' => false, 'message' => 'Route not saved'];
    }

    public static function routeUpdate($data, $id) {
        $data['updated_at'] = Carbon::now();
        $pointId = [$id, $data['from'], $data['to']];

        foreach($pointId as $valId) {
            $route = DB::table(self::$tableRoutes)->find($valId);
            if($route == null) {
                return ['success' => false, 'message' => 'Data not found in id of '.$valId];
            }
        }

        $route =  DB::table(self::$tableRoutes)
        ->where([
            [self::$tableRoutes.'.id', '=', $id],
            [self::$tableRoutes.'.from', '=', $data['from']],
            [self::$tableRoutes.'.to', '=', $data['to']]
        ])->get();

        if($route->count()) {
            if($route[0]->time != $data['time'] || $route[0]->cost != $data['cost']) {
                
            
                if(DB::table(self::$tableRoutes)->where('id', $id)->update($data)) {
                    return ['success' => true, 'message' => 'Data successfully updated'];
                }
                return ['success' => false, 'message' => 'Data not updated'];
            }
            else {
                return ['success' => false, 'message' => 'No data to be updated'];
            }
        }
        else {
            if(DB::table(self::$tableRoutes)->where('id', $id)->update($data)) {
                return ['success' => true, 'message' => 'Data successfully updated'];
            }
            return ['success' => false, 'message' => 'Data not updated'];
        }
    }

    public static function routeDestroy($id) {
        $route = DB::table(self::$tableRoutes)->find($id);

        if($route == null) {
            return ['success' => false, 'message' => 'Data not exist in id of '.$id];
        }

        if(DB::table(self::$tableRoutes)->where('id', $id)->delete()) {
            return ['success' => true, 'message' => ''];
        }
        return ['success' => false, 'message' => 'Data not deleted'];
    }

    public static function routeChecker($data) {
        $pointRoutesDB = [];
        $pointRoutesGraph = [];
        $origin = strToUpper($data['origin']);
        $destination = strToUpper($data['destination']);
        $points = DB::table(self::$tablePoints)->get();

        foreach($points as $key => $row) {
            $routesPoint = DB::table(self::$tableRoutes)
            ->join('points', 'routes.to', '=', 'points.id')
            ->select('points.name', 'routes.time', 'routes.cost')
            ->where('routes.from', $row->id)
            ->get();
           
            $routesPointListGraph = array();
            $routesPointListDB = array();

            foreach($routesPoint as $rowKey => $rowValue) {
                $routesPointListGraph[$rowValue->name] = $rowValue->time;
                $routesPointListDB[$rowValue->name] = array(
                    'time' => $rowValue->time,
                    'cost' => $rowValue->cost
                );
            } 
            $pointRoutesGraph[$row->name] = $routesPointListGraph;
            $pointRoutesDB[$row->name] = $routesPointListDB;
        }

        $algorithm = new Dijkstra($pointRoutesGraph);
        $paths = $algorithm->shortestPaths($origin, $destination);

        if(count($paths) == 1) {
            $data = self::getCostTime($pointRoutesDB, $paths);
            return $data;
        }
        else if(count($paths) == 2) {
            $data = self::getBestRouteCost($pointRoutesDB, $paths);
            return $data;
        }
        else {
            return [];
        }
    }

    public static function getBestRouteCost($graph, $paths) {
        $routeList = [];
        $bestRoute = [];
        $maxCost = 0;

        foreach($paths as $key => $path) {
            array_push($routeList, self::getCostTime($graph, $path));
        }

        foreach($routeList as $key => $route) {
            if($maxCost < $route['total_cost']) {
                $bestRoute = $route;
            }
        }

        return $bestRoute;
    }

    public static function getCostTime($graph, $path) {
        $totalTime = 0;
        $totalCost = 0;
        $best_route = '';
        $route_path = [];
        
        foreach($path as $point) {
            foreach($point as $key => $value) {
                $best_route .= $point[$key];

                if($key < count($point) - 1) {
                    $best_route .= ' => ';
                }

                if(count($point) > $key + 1) {

                    $firstKey = $point[$key];
                    $secondkey = $point[$key + 1];
                    $route_path[$firstKey.' => '.$secondkey]= $graph[$firstKey][$secondkey];

                    $totalTime  += $graph[$firstKey][$secondkey]['time'];
                    $totalCost  += $graph[$firstKey][$secondkey]['cost'];
                }
            }
        }
        return [
            'total_cost' => $totalCost,
            'total_time' => $totalTime,
            'best_route' => $best_route,
            'routes' => $route_path
        ];
    }
}
