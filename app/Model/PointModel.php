<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class PointModel extends Model
{
    protected $table = 'points';
    public static $tablePoints= 'points';

    public static function pointList() {
        return DB::table(self::$tablePoints)->get();
    }

    public static function pointShow($id) {
        return DB::table(self::$tablePoints)->find($id);
    }

    public static function pointStore($data) {
        $data['created_at'] = Carbon::now();
        if(DB::table(self::$tablePoints)->insert($data)) {
            return true;
        }
        return false;
    }

    public static function pointUpdate($data, $id) {
        $data['updated_at'] = Carbon::now();
        $point = DB::table(self::$tablePoints)->find($id);

        if($point == null) {
            return ['success' => false, 'message' => 'Data not found in id of '.$id];
        }

        if(DB::table(self::$tablePoints)->where('id', $id)->update($data)) {
            return ['success' => true, 'message' => ''];
        }
        return ['success' => false, 'message' => 'Data not updated'];
    }

    public static function pointDestroy($id) {
        $point = DB::table(self::$tablePoints)->find($id);

        if($point == null) {
            return ['success' => false, 'message' => 'Data not exist in id of '.$id];
        }

        foreach(['from', 'to'] as $field) {
            $route = DB::table('routes')->where($field, '=', $id)->get();

            if($route->count()) {
                return ['success' => false, 'message' => 'This point is used in routes'];
            }
        }
        
        if(DB::table(self::$tablePoints)->where('id', $id)->delete()) {
            return ['success' => true, 'message' => ''];
        }
        return ['success' => false, 'message' => 'Data not deleted'];
    }
}
