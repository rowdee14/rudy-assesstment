<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'AuthController@register');
Route::post('/login', 'AuthController@login');

Route::get('/profile', 'UserController@profileShow');
Route::put('profile', 'UserController@profileUpdate');
Route::get('/users', 'UserController@list');
Route::get('/user/{id}', 'UserController@show');
Route::put('/user/{id}', 'UserController@update');
Route::delete('/user/{id}', 'UserController@destroy');


Route::get('/points', 'PointController@list');
Route::get('/point/{id}', 'PointController@show');
Route::post('/point', 'PointController@store');
Route::put('/point/{id}', 'PointController@update');
Route::delete('/point/{id}', 'PointController@destroy');

Route::get('/routes', 'RouteController@list');
Route::post('/route/checker', 'RouteController@checker');
Route::get('/route/{id}', 'RouteController@show');
Route::post('/route', 'RouteController@store');
Route::put('/route/{id}', 'RouteController@update');
Route::delete('/route/{id}', 'RouteController@destroy');
